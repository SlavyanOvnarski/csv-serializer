#include <time.h>
#include <uuid/uuid.h>
#include <stdio.h>

/*
sets the logger to be silent
input: int
return: int
*/

static int my_silent = 0;
void setSilent(int silent){
    my_silent = silent;
}

/*
shows the message to the console
input: char*, char*
return: none
*/

void logger(char *fileName, char *message)
{
    if (my_silent == 0){
        time_t rawtime;
        struct tm *timeinfo;
        char buffer[80];

        time(&rawtime);
        timeinfo = localtime(&rawtime);

        uuid_t uuid;
        uuid_generate(uuid);

        char uuid_str[37];
        uuid_unparse(uuid, uuid_str);

        strftime(buffer, 80, "%Y/%m/%d %H:%M:%S", timeinfo);
        printf("%s [ %s ] [ %s ] %s\n", buffer, fileName, uuid_str, message);
    }
}
