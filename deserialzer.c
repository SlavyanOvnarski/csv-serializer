#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "rowStruct.c"

void logger(char *fileName, char *message);
/*
reads the data from the binary file, decompresses it and saves it to a new CSV file
input: char*, char*
return: none
*/
int read_data(char *binFileName)
{
    logger("deserializer.c", "Opening binary file");
    // Check if the file extension is BIN
    const char *binExtension = "bin";
    const char *fileExtension = strrchr(binFileName, '.');
    if (fileExtension == NULL || strcasecmp(fileExtension + 1, binExtension) != 0)
    {
        printf("Error: Invalid file extension. Only BIN files are supported.\n");
        return 3;
    }

    // Open the binary file for reading
    FILE *binaryFile = fopen(binFileName, "rb");
    if (!binaryFile)
    {
        printf("Error: failed to open %s\n", binFileName);
        return 1;
    }
    logger("deserializer.c", "Opening CSV file");

    // Create the CSV file for writing
    FILE *csvFile = fopen("decompressed.csv", "w");
    if (!csvFile)
    {
        printf("Error: failed to create decompressed.csv\n");
        fclose(binaryFile);
        return 2;
    }
    logger("deserializer.c", "Writting data from binary to CSV file");

    // Write the header row to the CSV file
    fprintf(csvFile, "\"Row ID\",\"Order ID\",\"Order Date\",\"Customer ID\",\"City\",\"State\",\"Postal Code\",\"Region\",\"Product ID\",\"Category\",\"Sub-Category\",\"Price\"\n");

    // Read each row of the binary file and write it to the CSV file
    short rowId = 1;
    fseek(binaryFile, 0, SEEK_END);
    int fileSize = ftell(binaryFile);
    fseek(binaryFile, 0, SEEK_SET);
    char *fileBuffer = malloc(fileSize);
    fread(fileBuffer, fileSize, 1, binaryFile);
    int fileIndex = 0;
    while (fileIndex < fileSize)
    {
        Row row = {0};
        row.rowId = rowId;
        int orderLength = strlen(&fileBuffer[fileIndex]);
        memcpy(row.order_id, &fileBuffer[fileIndex], orderLength);
        fileIndex += orderLength + 1;

        int dateLength = strlen(&fileBuffer[fileIndex]);
        memcpy(row.order_date, &fileBuffer[fileIndex], dateLength);
        fileIndex += dateLength + 1;

        int customerIdLength = strlen(&fileBuffer[fileIndex]);
        memcpy(row.customer_id, &fileBuffer[fileIndex], customerIdLength);
        fileIndex += customerIdLength + 1;

        int cityLength = strlen(&fileBuffer[fileIndex]);
        memcpy(row.city, &fileBuffer[fileIndex], cityLength);
        fileIndex += cityLength + 1;

        int stateLength = strlen(&fileBuffer[fileIndex]);
        memcpy(row.state, &fileBuffer[fileIndex], stateLength);
        fileIndex += stateLength + 1;

        int postalCodeLength = sizeof(row.postal_code);
        memcpy(&row.postal_code, &fileBuffer[fileIndex], postalCodeLength);
        fileIndex += postalCodeLength;

        int regionLength = strlen(&fileBuffer[fileIndex]);
        memcpy(row.region, &fileBuffer[fileIndex], regionLength);
        fileIndex += regionLength + 1;

        int productIdLength = strlen(&fileBuffer[fileIndex]);
        memcpy(row.product_id, &fileBuffer[fileIndex], productIdLength);
        fileIndex += productIdLength + 1;

        int categoryLength = strlen(&fileBuffer[fileIndex]);
        memcpy(row.category, &fileBuffer[fileIndex], categoryLength);
        fileIndex += categoryLength + 1;

        int subCategoryLength = strlen(&fileBuffer[fileIndex]);
        memcpy(row.sub_category, &fileBuffer[fileIndex], subCategoryLength);
        fileIndex += subCategoryLength + 1;

        int priceLength = sizeof(row.price);
        memcpy(&row.price, &fileBuffer[fileIndex], priceLength);
        fileIndex += priceLength;

        fprintf(csvFile, "%d,\"%s\",%s,\"%s\",\"%s\",\"%s\",%d,\"%s\",\"%s\",\"%s\",\"%s\",%.2f\n",
                row.rowId, row.order_id, row.order_date, row.customer_id, row.city, row.state,
                row.postal_code, row.region, row.product_id, row.category, row.sub_category, row.price);
        rowId += 1;
    }
    logger("deserializer.c", "Finished writting to CSV file, closing files");

    // Close the files
    fclose(binaryFile);
    fclose(csvFile);
    return 0;
}