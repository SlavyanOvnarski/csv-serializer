#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int silent = 0;
// serialize
// deserialize
static char *command;

// serializer.c
int write_data(char *csvFileName);

// deserializer.c

void setSilent(int silent);
void read_data(char *fileName);
/*
reads the user input
input: char**, int, char*
return: none
*/
void input(char **file, int argc, char *argv[])
{
   int hasFilePath = 0;
   int hasCommand = 0;

   for (int arg = 1; arg < argc; arg++)
   {
      printf("arg[%d] = %s\n", arg, argv[arg]);
      switch ((argv[arg])[0])
      {
      // set the logger to silent
      // s
      case 's':
         silent = 1;
         setSilent(silent);
         break;
      // set the path to the file
      // f path/to/file.csv
      case 'f':
         arg++;
         *file = argv[arg];
         char *extension = strrchr(*file, '.');
         if (extension == NULL || (strcmp(extension, ".csv") != 0 && strcmp(extension, ".bin") != 0))
         {
            printf("Invalid file extension. Only CSV and binary files are allowed.\n");
            return;
         }
         hasFilePath = 1;
         break;
      // select the mode
      // c serialize / deserialize
      case 'c':
         arg++;
         //*command = argv[arg];
         command = (argv[arg]);
         int isSerialize = strcmp(command, "serialize");
         int isDeserialize = strcmp(command, "deserialize");
         if (isSerialize == 0)
         {
            printf("command is serialize");
            write_data(*file);
         }
         else if (isDeserialize == 0)
         {
            printf("command is deserialize");
            read_data(*file);
         }
         else
         {
            printf("Unknown command");
         }
         hasCommand = 1;
         break;
      }
   }

   // Check if both file path and command are provided
   if (!hasFilePath || !hasCommand)
   {
      printf("File path and command are obligatory.\n");
   }
}

int main(int argc, char *argv[])
{
   char *fileName = "test.csv";
   input(&fileName, argc, argv);
   // get if it has to be silent and pass it to the logger
   // get the command
   // if command == serialize
   // call serialize file
   // else if command == deserialize
   // call deserialize file
   // else
   // unknown command
   return 0;
}
