/*
structure for the csv files
*/
typedef struct
{
    // "Row ID" =>1,2,
    short rowId;
    // "Order ID" => "CA-2016-152156", "CA-2016-152156"
    char order_id[20];
    // "Order Date" => 11/8/2016
    char order_date[12];
    // "Customer ID" => "CG-12520","CG-12520"
    char customer_id[10];
    // "City" => "Henderson", "Henderson"
    char city[255];
    // "State" => "Kentucky", "Kentucky"
    char state[255];
    // "Postal Code" => 42420, 42420
    int postal_code;
    // "Region" => "South"
    char region[255];
    //"Product ID" => "FUR-BO-10001798"
    char product_id[255];
    // "Category" => "Furniture"
    char category[255];
    // "Sub-Category" => "Bookcases", "Chairs"
    char sub_category[255];
    // "Price" => 261.96,731.94

    float price;
} Row;
