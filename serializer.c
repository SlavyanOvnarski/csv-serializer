#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "rowStruct.c"

void logger(char *fileName, char *message);

/*
reads the csv file, compresses the data and saves it to a binary file
input: char*
return: int
*/
int write_data(char *csvFileName)
{
    logger("serializer.c", "Opening CSV file");
    // Open the CSV file for reading
    FILE *csvFile = fopen(csvFileName, "r");
    if (!csvFile)
    {
        printf("Error: failed to open %s\n", csvFileName);
        return 1;
    }
    logger("serializer.c", "Opening Binary file");
    // Check if the file extension is CSV
    const char *csvExtension = "csv";
    const char *fileExtension = strrchr(csvFileName, '.');
    if (fileExtension == NULL || strcasecmp(fileExtension + 1, csvExtension) != 0)
    {
        printf("Error: Invalid file extension. Only CSV files are supported.\n");
        return 3;
    }

    // Create the binary file for writing
    FILE *binaryFile = fopen("output.bin", "wb");
    if (!binaryFile)
    {
        printf("Error: failed to create compressed.bin\n");
        fclose(csvFile);
        return 2;
    }

    // Skip the header row of the CSV file
    char header[200];
    logger("serializer.c", "Getting headers of the file");

    fgets(header, 200, csvFile);
    size_t headerLength = strlen(header);
    if (headerLength > 0 && header[headerLength - 1] == '\n')
    {
        header[headerLength - 1] = '\0';
    }

    // Compare the header with the expected value
    const char *expectedHeader = "\"Row ID\",\"Order ID\",\"Order Date\",\"Customer ID\",\"City\",\"State\",\"Postal Code\",\"Region\",\"Product ID\",\"Category\",\"Sub-Category\",\"Price\"";
    if (strcmp(header, expectedHeader) != 0)
    {
        printf("Header does not match the expected value.\n");
        return 4;
    }
    logger("serializer.c", "Start reading rows and writting to binary");

    // Read each row of the CSV file and write it to the binary file
    Row row;
    memset(&row, '\0', sizeof(row));
    while (fscanf(csvFile, "%hu,%[^,],%[^,],%[^,],%[^,],%[^,],%d,%[^,],%[^,],%[^,],%[^,],%f",
                  &row.rowId, row.order_id, row.order_date, row.customer_id, row.city, row.state,
                  &row.postal_code, row.region, row.product_id, row.category, row.sub_category, &row.price) == 12)
    {

        int zero = 0;
        fwrite(row.order_id + 1, strlen(row.order_id) - 2, 1, binaryFile);
        fwrite(&zero, 1, 1, binaryFile);
        fwrite(row.order_date, strlen(row.order_date), 1, binaryFile);
        fwrite(&zero, 1, 1, binaryFile);
        fwrite(row.customer_id + 1, strlen(row.customer_id) - 2, 1, binaryFile);
        fwrite(&zero, 1, 1, binaryFile);
        fwrite(row.city + 1, strlen(row.city) - 2, 1, binaryFile);
        fwrite(&zero, 1, 1, binaryFile);
        fwrite(row.state + 1, strlen(row.state) - 2, 1, binaryFile);
        fwrite(&zero, 1, 1, binaryFile);
        fwrite(&row.postal_code, sizeof(row.postal_code), 1, binaryFile);
        fwrite(row.region + 1, strlen(row.region) - 2, 1, binaryFile);
        fwrite(&zero, 1, 1, binaryFile);
        fwrite(row.product_id + 1, strlen(row.product_id) - 2, 1, binaryFile);
        fwrite(&zero, 1, 1, binaryFile);
        fwrite(row.category + 1, strlen(row.category) - 2, 1, binaryFile);
        fwrite(&zero, 1, 1, binaryFile);
        fwrite(row.sub_category + 1, strlen(row.sub_category) - 2, 1, binaryFile);
        fwrite(&zero, 1, 1, binaryFile);
        fwrite(&row.price, sizeof(row.price), 1, binaryFile);
        memset(&row, '\0', sizeof(row));
    }
    logger("serializer.c", "Finished reading rows and writting to binary, closing files");

    // Close the files
    fclose(csvFile);
    fclose(binaryFile);
    return 0;
}
